<?php

class mods_xml_parser {
  private $parser;
  private $format;
  private $node;
  private $keyword_count;
  private $contributors;
  private $contrib_count;
  private $name;
  private $name_type;
  private $name_part_type;
  private $role;
  private $titles;
  private $periodical;
  private $dates;
  private $urls;
  private $font_attr;
  private $session_id;
  private $batch_proc;
  private $terms;
  private $nids;
  private $dups;
  private $ident_type;
  private $title_type;
  private $start_page;
  private $end_page;
  private $field;
  private $in_related_item;
  private $in_origin_info;
  private $date_encoding;
  private $identifier;

  function parse_dom_record($dom) {
    $xml = $dom->ownerDocument->saveXML($dom);
    return $this->parse_xml_record($xml);
  }

  function parse_xml_record(&$xml, $identifier = '') {
    $this->identifier = substr(strrchr($identifier, ":"), 1);
    $this->parser = drupal_xml_parser_create($xml);
    xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, FALSE);
    xml_parser_set_option($this->parser, XML_OPTION_SKIP_WHITE, TRUE);
    xml_set_object($this->parser, $this);
    xml_set_element_handler($this->parser, 'mods_xml_startElement', 'mods_xml_endElement');
    xml_set_character_data_handler($this->parser, 'mods_xml_characterData');

     xml_parse($this->parser, $xml);
    return $this->node;
  }

  function parse_file($file, $terms, $batch, $session_id) {
    $this->terms = $terms;
    $this->batch_proc = $batch;
    $this->session_id = $session_id;
    $this->nids = array();
    $this->dups = array();

    if (!($fp = fopen($file->filepath, "r"))) {
      drupal_set_message(t("could not open XML input"), 'error');
      return;
    }
    $data = fread($fp, 2048);

    $this->parser = drupal_xml_parser_create($data);
    xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, FALSE);
    xml_parser_set_option($this->parser, XML_OPTION_SKIP_WHITE, TRUE);
    xml_set_object($this->parser, $this);
    xml_set_element_handler($this->parser, 'mods_xml_startElement', 'mods_xml_endElement');
    xml_set_character_data_handler($this->parser, 'mods_xml_characterData');

    xml_parse($this->parser, $data, feof($fp));

    while ($data = fread($fp, 2048)) {
      // $data = fread($fp, 2048);
      set_time_limit(30);
      if (!xml_parse($this->parser, $data, feof($fp))) {
        drupal_set_message(sprintf("XML error: %s at line %d",
        xml_error_string(xml_get_error_code($this->parser)),
        xml_get_current_line_number($this->parser)), 'error');
      }
    }
    xml_parser_free($this->parser);

    fclose($fp);

    return array($this->nids, $this->dups);

  }

  function mods_xml_startElement($parser, $name, $attrs) {
    $this->element = $name;
    switch ($name) {
      case 'mods':
        $this->node = new stdClass;
        break;
      case 'name':
        $this->name = array();
        $this->role = '';
        $type = isset($attrs['type']) ? $attrs['type'] : '';
        if ($type == 'personal') {
          $this->name_type = 1;
        }
        elseif ($type == 'corporate') {
          $this->name_type = 5;
        }
        elseif ($type == 'conference') {
          $this->name_type = 'conference';
        }
        break;
      case 'namePart':
        $type = isset($attrs['type']) ? $attrs['type'] : '';
        switch ($type) {
          case 'family':
            $this->name_part_type = 'lastname';
            break;
          case 'given':
            $this->name_part_type = 'firstname';
            break;
          case 'date':
            $this->name_part_type = 'date';
            break;
          case 'termsOfAddress':
            $this->name_part_type = '';
            break;
          default:
            $this->name_part_type = 'name';
        }
        break;
      case 'note':
        break;
      case 'identifier':
        $type = isset($attrs['type']) ? $attrs['type'] : '';
        switch ($type) {
          case 'doi':
            $this->field = 'biblio_doi';
            break;
          case 'nrcc_number':
            $this->field = 'biblio_other_number';
            break;
          case 'isbn':
            $this->field = 'biblio_isbn';
            break;
          case 'issn':
            $this->field = 'biblio_issn';
            break;
        }
        break;
      case 'subject':
        $this->keyword_count = 0;
        break;
      case 'abstract':
        $this->field = 'biblio_abst_e';
        break;
      case 'institute':
        $id = isset($attrs['ID']) ? $attrs['ID'] : '';
        if (!empty($id)) {
          $this->node->institute[] = $id;
        }
        break;
      case 'titleInfo':
        $this->title_type = isset($attrs['type']) ? $attrs['type'] : '';
        break;
      case 'title':
        switch ($this->title_type) {
          case 'abbreviated':
            $this->field = 'biblio_short_title';
            break;
          case 'abbreviated':
            $this->field = 'biblio_short_title';
            break;
          case 'translated':
            $this->field = 'biblio_translated_title';
            break;
          case 'uniform':
            $this->field = 'biblio_short_title';
            break;
          default:
            if ($this->in_related_item) {
              $this->field = 'biblio_secondary_title';
            }
            else {
              $this->field = 'title';
            }
             break;
        }
        break;
      case 'relatedItem':
        $this->in_related_item = TRUE;
        break;
      case 'url':
        $this->field = 'biblio_url';
        break;
      case 'part':
        $this->inpart = TRUE;
        break;
      case 'detail':
        $type = isset($attrs['type']) ? $attrs['type'] : '';
        switch ($type) {
          case 'volume':
            $this->field = 'biblio_volume';
            break;
          case 'issue':
            $this->field = 'biblio_issue';
            break;
          case 'month':
            $this->field = 'biblio_date';
            break;
        }
        break;
      case 'extent':
        $unit = isset($attrs['unit']) ? $attrs['unit'] : '';
        $this->field = 'biblio_pages';
        $this->node->biblio_pages = '';
        break;
      case 'genre':
        $this->field = 'biblio_type';
        break;
      case 'dateIssued':
        $this->date_encoding = isset($attrs['encoding']) ? $attrs['encoding'] : '';
        break;
      case 'file':
        if (isset($attrs['type']) && $attrs['type'] == 'main' && isset($attrs['displayLabel'])) {
          $this->mods_xml_build_file_object($attrs['displayLabel']);
        }
        break;
    }

  }
  function mods_xml_endElement($parser, $name) {
    switch ($name) {
      case 'name':
        if ($this->role == 'aut' && $this->name_type ) {
          $this->name['auth_type'] = $this->name_type;
          if (!isset($this->name['name']) && isset($this->name['lastname'])) {
            $this->name['name'] = $this->name['lastname'];
            $this->name['name'] .= isset($this->name['firstname']) ? ', ' . $this->name['firstname'] : '';
          }
          $this->node->biblio_contributors[$this->name_type][] = $this->name;
        }
        if ($this->name_type == 'conference') {
          $this->node->biblio_secondary_title = $this->name['name'];
          $this->node->biblio_date = isset($this->name['date']) ? trim($this->name['date']) : '';
          if (strlen($this->node->biblio_date) > 3)
           if (is_numeric( substr($this->node->biblio_date, -4))) {
             $this->node->biblio_year = substr($this->node->biblio_date, -4);
           }
           elseif (is_numeric( substr($this->node->biblio_date, 0, 4))) {
            $this->node->biblio_year = substr($this->node->biblio_date, 0, 4);
          }
        }
        $this->name_type = '';
        $this->role = '';
        break;
      case 'namePart':
        $this->name_part_type = '';
        break;
      case 'topic':
        $this->keyword_count++;
        break;
      case 'relatedItem':
        $this->in_related_item = FALSE;
        break;
      case 'extent':
        if ($this->field == 'biblio_pages') {
          $this->node->biblio_pages = $this->start_page . ' - ' . $this->end_page;
        }
        $this->start_page = '';
        $this->end_page = '';
        $this->field = '';
        break;
      case 'detail':
      case 'identifier':
      case 'title':
      case 'url':
      case 'abstract':
        $this->field = '';
        break;
      case 'titleInfo':
        $this->title_type = '';
        break;
      case 'genre':
        switch ($this->node->biblio_type) {
          case 'article':
            $this->node->biblio_type = 102;
            break;
          case 'bibliography':
            $this->node->biblio_type = 100;
            break;
          case 'conference publication':
            $this->node->biblio_type = 103;
            break;
          case 'report':
            $this->node->biblio_type = 109;
            break;
          case 'book chapter':
            $this->node->biblio_type = 101;
            break;
          case 'book':
            $this->node->biblio_type = 100;
            break;
            case 'patent':
            $this->node->biblio_type = 119;
            break;
          case 'presentation':
            $this->node->biblio_type = 135;
            break;
            default:
              $this->node->biblio_custom1 = $this->node->biblio_type;
              $this->node->biblio_type = 129;

            break;
        }
        $this->field = '';
        break;
      case 'dateIssued':
        $this->date_encoding =  '';
        break;

    }

  $this->element = '';
  }

  function mods_xml_characterData($parser, $data) {
    $data = trim($data);
    if (!empty($data)) {
      switch ($this->element) {
        case 'roleTerm':
          $this->role .= $data;
          break;
        case 'namePart':
          $this->name[$this->name_part_type] .= $data;
          break;
        case 'topic':
          $this->node->biblio_keywords[$this->keyword_count] .= $data;
          break;
        case 'start':
          $this->start_page .= $data;
          break;
        case 'end':
          $this->end_page .= $data;
          break;
        case 'dateIssued':
          if ($this->date_encoding == 'w3cdtf') {
            $this->node->biblio_year = substr($data, 0, 4);
            if (empty($this->node->biblio_date)) {
              $this->node->biblio_date = $data;
            }
          }
          break;
        default:
          if ($this->field) {
            $this->node->{$this->field} .= $data;
          }
      }
    }
  }

  function mods_xml_build_file_object($name) {
    global $user;
    $file = new stdClass();
    $file->filename = str_replace(' ','_', $name);
    $path = file_directory_path() . '/NPARC/';
    file_check_directory($path, FILE_CREATE_DIRECTORY);
    $file->filepath = file_create_path(file_directory_path() . '/NPARC/' . $file->filename);
    $file_contents = $this->curl_get('http://nparc.cisti-icist.nrc-cnrc.gc.ca/npsi/ctrl?action=rtdoc&article=0&fd=pdf&an=' . $this->identifier);
    if (!empty($file_contents)) {
      $file->filesize = file_put_contents($file->filepath, $file_contents);
      if ($file->filesize < 30000) {
        file_delete($file->filepath);
        return;
      }
    }
    $file->filemime = file_get_mimetype('nrcc.pdf');
    $file->uid = $user->uid;
    $file->status = FILE_STATUS_TEMPORARY;
    $file->timestamp = time();
    $file->description = $file->filename;
    $file->list = 1;
    $file->weight = 0;
    $file->new = TRUE;
    drupal_write_record('files', $file);
    $this->node->files[$file->fid] = $file;
  }
  function curl_get($url, array $get = array(), array $options = array())
  {
    $defaults = array(
    CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get),
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_TIMEOUT => 20
    );

    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    if( ! $result = curl_exec($ch))
    {
      trigger_error(curl_error($ch));
    }
    curl_close($ch);
    sleep(1);

    return $result;
  }

}